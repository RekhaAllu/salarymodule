import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor() { }

private data = [];
private users = ["aaaa","bbbb","cccc"];

 addRecord(record) {
    this.data.push(record);
 }

fetchRecords() {
  return this.data;
}

getUser() {
  return this.users;
}
  
}