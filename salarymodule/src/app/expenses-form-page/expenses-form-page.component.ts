import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl,Validators } from '@angular/forms';
import { DataService } from '../data.service';

@Component({
  selector: 'app-expenses-form-page',
  templateUrl: './expenses-form-page.component.html',
  styleUrls: ['./expenses-form-page.component.scss']
})
/** class for expensesform page */
export class ExpensesFormPageComponent implements OnInit {
  group: FormGroup;
  errorMessage='';



  constructor(private _dataService: DataService) { }
  users = [];

  ngOnInit() {
    /**
   * @description it will add elements in the form.
   * @author bhargavi 
   */ 
    this.group= new FormGroup({
      'amount': new FormControl('',[Validators.required]),
      'description' : new FormControl('',[Validators.required]),
      'spentBy' : new FormControl('',[Validators.required]),
      'date' : new FormControl('',[Validators.required])
    });
  }


  /**
   * @description function for the given validation.
   * @author bhargavi 
   */ 
  addForm(){
    if(this.group.valid){
      let value = this.group.value;
      value['type'] = 'expenses';
      this._dataService.addRecord(value);
      this.group.reset();
    }   
  }
}
